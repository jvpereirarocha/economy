from django.db import models
from ..base import models as base
from ..base import validators as valid
# Create your models here.


class Expenses(base.Base):

    CATEGORY_TYPES = [
        ('CHOOSE', 'CHOOSE'),
        ('FOOD', 'FOOD'),
        ('TAXES', 'TAXES'),
        ('PAYMENTS', 'PAYMENTS'),
        ('ENTERTAINMENT', 'ENTERTAINMENT'),
        ('EDUCATION', 'EDUCATION'),
        ('TRANSPORTATION', 'TRANSPORTATION'),
        ('HEALTH', 'HEALTH'),
        ('OTHER', 'OTHER'),
    ]


    category = models.CharField(max_length=50, choices=CATEGORY_TYPES,\
                                default='CHOOSE', validators=[valid.validate_select])

    expense_date = models.DateField(validators=[valid.validate_date])

    class Meta:
        verbose_name = 'Expense'
        verbose_name_plural = 'Expenses'

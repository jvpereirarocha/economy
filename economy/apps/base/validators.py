from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import datetime


def validate_description(description):
    only_text = description.isalpha()
    if len(description) > 255 or not only_text:
        raise ValidationError(_('Invalid description'),)

def validate_select(type):
    if type is None or type == 'CHOOSE':
        raise ValidationError(_('This field must have a value'))

def validate_date(date):
    if date > datetime.date.today():
        raise ValidationError(_("Invalid Date"))

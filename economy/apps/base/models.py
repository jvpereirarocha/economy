from django.db import models
from . import validators as valid

class Base(models.Model):

    TYPES = [
        ('CHOOSE', 'CHOOSE'),
        ('FIXED', 'FIXED'),
        ('VARIABLE', 'VARIABLE')
    ]


    description = models.CharField(max_length=255, verbose_name='Description',\
                                    validators=[valid.validate_description])

    type = models.CharField(max_length=30, choices=TYPES, default='CHOOSE',\
                            validators=[valid.validate_select])

    class Meta:
        verbose_name = 'Base'
        verbose_name_plural = 'Bases'
